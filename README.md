### Dash Modeling Format (.dmf)

This the version 0 initial development environment for the Dash Model Format.
It has been deprecated and current development can be found here: https://gitlab.com/dashgl/format

## Specification

TEX offset TEX offset
MAT offset MAT offset
BONE offset POS offset
NORM offset CLR offset
UV offset FACE offset 
FACE offset ANIM offset
ANIM offset ANIM ofset

POS will include weights and indexes only is BONE exists
This is assumed behavior

FACE definition is [mat index (2)][number tri (2)]
And then pairs of 3 for all of the attributes declared after bone.
So if ust pos and uv0, then abc, auv, buv, cuv and repeat for each vertex

Names for TEX, ANIM and BONE should be declared inside each block. Name attribute
followed by the string length, followed by the string (padding to 4 bytes)

(MAGIC) (length) (flags) (number of elements)
